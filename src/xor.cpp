// XOR

#include <systemc.h>
#include "xor.hpp"

using namespace std;

void XOR_MODULE::XOR_MODULE_op(){	

	sc_lv<16> a,b,z;	

	while(true){
		wait();
		a = XOR_in1->read();		
		b = XOR_in2->read();
		
		for(unsigned i=0; i<16; i++) {
			z[i] = a[i] ^ b[i];
		}

		XOR_out1->write(z);
	}
}
