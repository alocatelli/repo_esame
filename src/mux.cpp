// multiplexer 16bit

#include <systemc.h>
#include "mux.hpp"

using namespace std;

void MUX_MODULE::MUX_MODULE_op(){	

	sc_lv<16> in1,in2,in3,in4,in5,in6,in7,in8;
	sc_lv<16> out1;
	sc_uint<3> sel;	

	while(true){
		wait();
		in1 = MUX_in1->read();
		in2 = MUX_in2->read();
		in3 = MUX_in3->read();
		in4 = MUX_in4->read();
		in5 = MUX_in5->read();
		in6 = MUX_in6->read();
		in7 = MUX_in7->read();
		in8 = MUX_in8->read();

		sel = MUX_sel.read().to_uint();		
		
		switch(sel) {
			case 0 : out1 = in1;break;
			case 1 : out1 = in2;break;
			case 2 : out1 = in3;break;
			case 3 : out1 = in4;break;
			case 4 : out1 = in5;break;
			case 5 : out1 = in6;break;
			case 6 : out1 = in7;break;
			case 7 : out1 = in8;break;
		}

		MUX_out1->write(out1);
	}
}
