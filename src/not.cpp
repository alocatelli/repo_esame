// NOT 

#include <systemc.h>
#include "not.hpp"

using namespace std;

void NOT_MODULE::NOT_MODULE_op(){	

	sc_lv<16> a,z;

	while(true){
		wait();
		a = NOT_in1->read();
		for(unsigned i=0; i<16; i++){
			z[i] = ~a[i];
		}

//		z = ~a;
		NOT_out1->write(z);
	}
}
