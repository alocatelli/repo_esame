#include <systemc.h>
#include "nor.hpp"

using namespace std;

SC_HAS_PROCESS(NOR_MODULE);

NOR_MODULE::NOR_MODULE(sc_module_name name): sc_module(name), or1("or1"), not1("not1") {

//Map modules ALU

//OR
	or1.OR_in1(NOR_in1);
	or1.OR_in2(NOR_in2);
	or1.OR_out1(OR_out);
//NOT
	not1.NOT_in1(OR_out);
	not1.NOT_out1(NOR_out1);	

}

