// AND

#include <systemc.h>
#include "and.hpp"

using namespace std;

void AND_MODULE::AND_MODULE_op(){	

	sc_lv<16> a,b,z;	

	while(true){
		wait();
		a = AND_in1->read();		
		b = AND_in2->read();
		
		for(unsigned i=0; i<16; i++) {
			z[i] = a[i] & b[i];
		}

		AND_out1->write(z);
	}
}
