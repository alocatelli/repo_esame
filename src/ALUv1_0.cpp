#include <systemc.h>
#include "ALUv1_0.hpp"

using namespace std;

SC_HAS_PROCESS(ALU_MODULE);

ALU_MODULE::ALU_MODULE(sc_module_name name): sc_module(name), and1("and1"), or1("or1"), not1("not1"), xor1("xor1"), nand1("nand1"), nor1("nor1"), shifter1("shifter1"), adder_sub1("adder_sub1"), mux1("mux1")  {

//Map modules ALU

//AND
	and1.AND_in1(ALU_in1);
	and1.AND_in2(ALU_in2);
	and1.AND_out1(AND_out);
//OR
	or1.OR_in1(ALU_in1);
	or1.OR_in2(ALU_in2);
	or1.OR_out1(OR_out);
//NOT
	not1.NOT_in1(ALU_in1);
	not1.NOT_out1(NOT_out);	
//XOR
	xor1.XOR_in1(ALU_in1);
	xor1.XOR_in2(ALU_in2);
	xor1.XOR_out1(XOR_out);
//NAND
	nand1.NAND_in1(ALU_in1);
	nand1.NAND_in2(ALU_in2);
	nand1.NAND_out1(NAND_out);
//NOR
	nor1.NOR_in1(ALU_in1);
	nor1.NOR_in2(ALU_in2);
	nor1.NOR_out1(NOR_out);
//SHIFTER
	shifter1.SHIFTER_in1(ALU_in1);
	shifter1.SHIFTER_out1(SHIFTER_out);
	shifter1.SHIFTER_sel(ALU_op_sel);
//ADDER_SUB
	adder_sub1.ADDER_SUB_in1(ALU_in1);
	adder_sub1.ADDER_SUB_in2(ALU_in2);
	adder_sub1.ADDER_SUB_out1(ADDER_SUB_out);
	adder_sub1.ADDER_SUB_sel(ALU_op_sel);
//MULTIPLEXER
	mux1.MUX_in1(AND_out);
	mux1.MUX_in2(OR_out);
	mux1.MUX_in3(NOT_out);
	mux1.MUX_in4(XOR_out);
	mux1.MUX_in5(SHIFTER_out);
	mux1.MUX_in6(ADDER_SUB_out);
	mux1.MUX_in7(NAND_out);
	mux1.MUX_in8(NOR_out);
	mux1.MUX_out1(ALU_out1);
	mux1.MUX_sel(ALU_sel);

}

