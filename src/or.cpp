// OR

#include <systemc.h>
#include "or.hpp"

using namespace std;

void OR_MODULE::OR_MODULE_op(){	

	sc_lv<16> a,b,z;	

	while(true){
		wait();
		a = OR_in1->read();		
		b = OR_in2->read();
		
		for(unsigned i=0; i<16; i++) {
			z[i] = a[i] | b[i];
		}

		OR_out1->write(z);
	}
}
