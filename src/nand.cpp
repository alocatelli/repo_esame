#include <systemc.h>
#include "nand.hpp"

using namespace std;

SC_HAS_PROCESS(NAND_MODULE);

NAND_MODULE::NAND_MODULE(sc_module_name name): sc_module(name), and1("and1"), not1("not1") {

//Map modules ALU

//AND
	and1.AND_in1(NAND_in1);
	and1.AND_in2(NAND_in2);
	and1.AND_out1(AND_out);
//NOT
	not1.NOT_in1(AND_out);
	not1.NOT_out1(NAND_out1);	

}

