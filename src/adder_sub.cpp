// adder_subtractor 16bit

#include <systemc.h>
#include <math.h>
#include "adder_sub.hpp"

using namespace std;

void ADDER_SUB_MODULE::ADDER_SUB_MODULE_op(){	

	sc_uint<16> a,b,z;
	bool sel;	

	while(true){
		wait();
		a = ADDER_SUB_in1.read().to_uint();		
		b = ADDER_SUB_in2.read().to_uint();
		
		sel = ADDER_SUB_sel->read();		
		
		if(sel == SC_LOGIC_0) {
			z = a + b;
		}
		else if(sel == SC_LOGIC_1){
			z = a - b;
		} 

		ADDER_SUB_out1->write(static_cast<sc_lv<16> >(z));
	}
}
