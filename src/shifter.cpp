// adder_subtractor 16bit

#include <systemc.h>
#include "shifter.hpp"

using namespace std;

void SHIFTER_MODULE::SHIFTER_MODULE_op(){	

	sc_lv<16> a,z;
	bool sel;	

	while(true){
		wait();
		a = SHIFTER_in1->read();		
		sel = SHIFTER_sel->read();		
		
		if(sel == SC_LOGIC_0) {
			z = a << 1;
		}
		else if(sel == SC_LOGIC_1) {
			z = a >> 1;
		} 

		SHIFTER_out1->write(z);
	}
}
