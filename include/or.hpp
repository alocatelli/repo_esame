// OR

#ifndef OR_MODULE_HPP
#define OR_MODULE_HPP

SC_MODULE(OR_MODULE){

	sc_in<sc_lv<16> > OR_in1;
	sc_in<sc_lv<16> > OR_in2;
	sc_out<sc_lv<16> > OR_out1;

	SC_CTOR(OR_MODULE){

		SC_THREAD(OR_MODULE_op);
		sensitive << OR_in1 << OR_in2;
	}

	void OR_MODULE_op();
};

#endif
