// 1bit shifter

#ifndef SHIFTER_MODULE_HPP
#define SHIFTER_MODULE_HPP

SC_MODULE(SHIFTER_MODULE){

	sc_in<sc_lv<16> > SHIFTER_in1;
	sc_out<sc_lv<16> > SHIFTER_out1;
	
	sc_in<bool> SHIFTER_sel;

	SC_CTOR(SHIFTER_MODULE){

		SC_THREAD(SHIFTER_MODULE_op);
		sensitive << SHIFTER_in1 << SHIFTER_sel;
	}

	void SHIFTER_MODULE_op();
};

#endif
