// NOR structural version

#ifndef NOR_MODULE_HPP
#define NOR_MODULE_HPP

// include logic modules
#include "or.hpp"
#include "not.hpp"


SC_MODULE(NOR_MODULE){

	sc_in<sc_lv<16> > NOR_in1,NOR_in2;
	sc_out<sc_lv<16> > NOR_out1;
	

	// Internal signals:
	sc_signal<sc_lv<16> > OR_out;

//
	OR_MODULE 		or1;
	NOT_MODULE 		not1;

//
	NOR_MODULE(sc_module_name  name);
//

};

#endif
