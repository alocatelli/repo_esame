// NOT

#ifndef NOT_MODULE_HPP
#define NOT_MODULE_HPP

SC_MODULE(NOT_MODULE){

	sc_in<sc_lv<16> > NOT_in1;
	sc_out<sc_lv<16> > NOT_out1;

	SC_CTOR(NOT_MODULE){

		SC_THREAD(NOT_MODULE_op);
		sensitive << NOT_in1;
	}

	void NOT_MODULE_op();
};

#endif


