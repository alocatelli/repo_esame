// XOR

#ifndef XOR_MODULE_HPP
#define XOR_MODULE_HPP

SC_MODULE(XOR_MODULE){

	sc_in<sc_lv<16> > XOR_in1;
	sc_in<sc_lv<16> > XOR_in2;
	sc_out<sc_lv<16> > XOR_out1;

	SC_CTOR(XOR_MODULE){

		SC_THREAD(XOR_MODULE_op);
		sensitive << XOR_in1 << XOR_in2;
	}

	void XOR_MODULE_op();
};

#endif
