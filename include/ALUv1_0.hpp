// ALU module version 1.0

#ifndef ALU_MODULE_HPP
#define ALU_MODULE_HPP

// logic modules
#include "and.hpp"
#include "or.hpp"
#include "not.hpp"
#include "xor.hpp"
#include "nand.hpp"
#include "nor.hpp"

//shifter
#include "shifter.hpp"

//mat
#include "adder_sub.hpp"

//multiplexer
#include "mux.hpp"

SC_MODULE(ALU_MODULE){

	sc_in<sc_lv<16> > ALU_in1,ALU_in2;
	sc_out<sc_lv<16> > ALU_out1;
	
	sc_in<sc_lv<3> > ALU_sel;
	sc_in<bool > ALU_op_sel;

	// Internal signals:
	sc_signal<sc_lv<16> > AND_out, OR_out, NOT_out, XOR_out, SHIFTER_out, ADDER_SUB_out, MUX_out, NAND_out, NOR_out;

//
	AND_MODULE 		and1;
	OR_MODULE  		or1;
	NOT_MODULE 		not1;
	XOR_MODULE 		xor1;
	NAND_MODULE		nand1;
	NOR_MODULE		nor1;	

	SHIFTER_MODULE 		shifter1;
	
	ADDER_SUB_MODULE 	adder_sub1;		

	MUX_MODULE 		mux1;

//
	ALU_MODULE(sc_module_name  name);
//

};

#endif
