// AND

#ifndef AND_MODULE_HPP
#define AND_MODULE_HPP

SC_MODULE(AND_MODULE){

	sc_in<sc_lv<16> > AND_in1;
	sc_in<sc_lv<16> > AND_in2;
	sc_out<sc_lv<16> > AND_out1;

	SC_CTOR(AND_MODULE){

		SC_THREAD(AND_MODULE_op);
		sensitive << AND_in1 << AND_in2;
	}

	void AND_MODULE_op();
};

#endif
