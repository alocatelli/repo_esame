// NAND structural version

#ifndef NAND_MODULE_HPP
#define NAND_MODULE_HPP

// include logic modules
#include "and.hpp"
#include "not.hpp"


SC_MODULE(NAND_MODULE){

	sc_in<sc_lv<16> > NAND_in1,NAND_in2;
	sc_out<sc_lv<16> > NAND_out1;
	

	// Internal signals:
	sc_signal<sc_lv<16> > AND_out;

//
	AND_MODULE 		and1;
	NOT_MODULE 		not1;

//
	NAND_MODULE(sc_module_name  name);
//

};

#endif
