// adder_subtractor 16bit
#ifndef ADDER_SUB_MODULE_HPP
#define ADDER_SUB_MODULE_HPP

SC_MODULE(ADDER_SUB_MODULE){

	sc_in<sc_lv<16> > ADDER_SUB_in1;
	sc_in<sc_lv<16> > ADDER_SUB_in2;
	sc_out<sc_lv<16> > ADDER_SUB_out1;
	
	sc_in<bool > ADDER_SUB_sel;

	SC_CTOR(ADDER_SUB_MODULE){

		SC_THREAD(ADDER_SUB_MODULE_op);
		sensitive << ADDER_SUB_in1 << ADDER_SUB_in2 << ADDER_SUB_sel;
	}

	void ADDER_SUB_MODULE_op();
};

#endif
