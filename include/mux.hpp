// multiplexer 16bit

#ifndef MUX_MODULE_HPP
#define MUX_MODULE_HPP

SC_MODULE(MUX_MODULE){

	sc_in<sc_lv<16> > MUX_in1,MUX_in2,MUX_in3,MUX_in4,MUX_in5,MUX_in6,MUX_in7,MUX_in8;
	sc_out<sc_lv<16> > MUX_out1;
	
	sc_in<sc_lv<3> > MUX_sel;

	SC_CTOR(MUX_MODULE){

		SC_THREAD(MUX_MODULE_op);
		sensitive << MUX_in1 << MUX_in2 << MUX_in3 << MUX_in4 << MUX_in5 << MUX_in6 << MUX_in7 << MUX_in8 << MUX_sel;
	}

	void MUX_MODULE_op();
};

#endif
