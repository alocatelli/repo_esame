# CMake generated Testfile for 
# Source directory: /home/andrea/Desktop/repo_esame1
# Build directory: /home/andrea/Desktop/repo_esame1/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(TB_NOT_MODULE "test_NOT_MODULE")
add_test(TB_OR_MODULE "test_OR_MODULE")
add_test(TB_AND_MODULE "test_AND_MODULE")
add_test(TB_XOR_MODULE "test_XOR_MODULE")
add_test(TB_ADDER_SUB_MODULE "test_ADDER_SUB_MODULE")
add_test(TB_SHIFTER_MODULE "test_SHIFTER_MODULE")
add_test(TB_MUX_MODULE "test_MUX_MODULE")
add_test(TB_NAND_MODULE "test_NAND_MODULE")
add_test(TB_NOR_MODULE "test_NOR_MODULE")
add_test(TB_ALU_MODULE "test_ALU_MODULE")
