#include <systemc.h>
#include <string>
#include <math.h>
#include "adder_sub.hpp"

using namespace std;

SC_MODULE(TB){

   public:	

	sc_signal<sc_lv<16> >	in1_test;
	sc_signal<sc_lv<16> >	in2_test;
	sc_signal<sc_lv<16> >	out1_test;
	
	sc_signal<bool >  sel_test;

	ADDER_SUB_MODULE	TB_ADDER_SUB_MODULE;
	

	SC_CTOR(TB):TB_ADDER_SUB_MODULE("TB_ADDER_SUB_MODULE"){

		SC_THREAD(test_adder_sub);
		TB_ADDER_SUB_MODULE.ADDER_SUB_in1(this -> in1_test);
		TB_ADDER_SUB_MODULE.ADDER_SUB_in2(this -> in2_test);
		TB_ADDER_SUB_MODULE.ADDER_SUB_sel(this -> sel_test);		
		TB_ADDER_SUB_MODULE.ADDER_SUB_out1(this -> out1_test);
		TEST_in_value();	}   

   private:

	static const int N_TEST = 2;
	sc_lv<16> TEST_in1[N_TEST], TEST_in2[N_TEST], TEST_out1[N_TEST];
	bool  TEST_sel[N_TEST];

	void test_adder_sub(){

		for (unsigned i=0;i<N_TEST;i++) {

			in1_test.write(TEST_in1[i]);
			cout << "ADDER_SUB_MODULE in1_test: " << TEST_in1[i] <<endl;
			in2_test.write(TEST_in2[i]);
			cout << "ADDER_SUB_MODULE in2_test: " << TEST_in2[i] <<endl;
			sel_test.write(TEST_sel[i]);
			cout << "ADDER_SUB_MODULE sel_test (0 ADDERR; 1 SUB): " << TEST_sel[i] <<endl;
			
			wait(5,SC_NS);	

			TEST_out1[i] = out1_test.read();

			cout << "return output ADDER_SUB: "<< TEST_out1[i] <<endl;
			cout << " " <<endl;
		}	
	}

	void TEST_in_value() {

		//Test somma, sel = 0
		TEST_in1[0] = 1;
		TEST_in2[0] = 1;
		TEST_sel[0] = 0;
		
		//Test sottrazione, sel = 1 		
		TEST_in1[1] = 3;
		TEST_in2[1] = 2;
		TEST_sel[1] = 1;
	}

};



int sc_main(int argc, char* argv[]) {

	TB test_adder_sub("test_adder_sub");
	cout << "Start test ADDER_SUB" << endl;

	sc_start();

	cout << "End test ADDER_SUB" << endl;  

	return 0;

}

