#include <systemc.h>
#include <string>
#include <math.h>
#include "mux.hpp"

using namespace std;

SC_MODULE(TB){

   public:	

	sc_signal<sc_lv<16> >	in1_test,in2_test,in3_test,in4_test,in5_test,in6_test,in7_test,in8_test;
	sc_signal<sc_lv<16> >	out1_test;
	
	sc_signal<sc_lv<3> >  sel_test;

	MUX_MODULE	TB_MUX_MODULE;
	

	SC_CTOR(TB):TB_MUX_MODULE("TB_MUX_MODULE"){

		SC_THREAD(test_mux);
		TB_MUX_MODULE.MUX_in1(this -> in1_test);
		TB_MUX_MODULE.MUX_in2(this -> in2_test);
		TB_MUX_MODULE.MUX_in3(this -> in3_test);
		TB_MUX_MODULE.MUX_in4(this -> in4_test);
		TB_MUX_MODULE.MUX_in5(this -> in5_test);
		TB_MUX_MODULE.MUX_in6(this -> in6_test);
		TB_MUX_MODULE.MUX_in7(this -> in7_test);
		TB_MUX_MODULE.MUX_in8(this -> in8_test);
		TB_MUX_MODULE.MUX_sel(this -> sel_test);		
		TB_MUX_MODULE.MUX_out1(this -> out1_test);
		TEST_in_value();	
	}   

   private:

	static const int N_TEST = 2;
	sc_lv<16> TEST_in1[N_TEST], TEST_in2[N_TEST], TEST_in3[N_TEST], TEST_in4[N_TEST], TEST_in5[N_TEST], TEST_in6[N_TEST], TEST_in7[N_TEST], TEST_in8[N_TEST], TEST_out1[N_TEST];
	sc_lv<3>  TEST_sel[N_TEST];


	void test_mux(){

		for (unsigned i=0;i<N_TEST;i++) {

			in1_test.write(TEST_in1[i]);
			in2_test.write(TEST_in2[i]);
			in3_test.write(TEST_in3[i]);
			in4_test.write(TEST_in4[i]);
			in5_test.write(TEST_in5[i]);
			in6_test.write(TEST_in6[i]);
			in7_test.write(TEST_in7[i]);
			in8_test.write(TEST_in8[i]);
			cout << "MUX_MODULE in_tests: " <<endl;
			cout <<  TEST_in1[i] <<endl;
			cout <<  TEST_in2[i] <<endl;
			cout <<  TEST_in3[i] <<endl;
			cout <<  TEST_in4[i] <<endl;
			cout <<  TEST_in5[i] <<endl;
			cout <<  TEST_in6[i] <<endl;
			cout <<  TEST_in7[i] <<endl;
			cout <<  TEST_in8[i] <<endl;

			sel_test.write(TEST_sel[i]);
			cout << "MUX_MODULE sel_test: " << TEST_sel[i] <<endl;
			
			wait(5,SC_NS);	

			TEST_out1[i] = out1_test.read();

			cout << "return output MUX: "<< TEST_out1[i] <<endl;
			cout << " " <<endl;
		}	
	}


	void TEST_in_value() {

		//Test 1
		TEST_in1[0] = "0000000000001001";
		TEST_in2[0] = "0000000000001010";
		TEST_in3[0] = "0000000000001011";
		TEST_in4[0] = "0000000000001100";
		TEST_in5[0] = "0000000000001101";
		TEST_in6[0] = "0000000000001110";
		TEST_in7[0] = "0000000000001111";
		TEST_in8[0] = "0000000000010000";
		TEST_sel[0] = 6;
		
		//Test 2		
		TEST_in1[1] = "0000000000001001";
		TEST_in2[1] = "0000000000001010";
		TEST_in3[1] = "0000000000001011";
		TEST_in4[1] = "0000000000001100";
		TEST_in5[1] = "0000000000001101";
		TEST_in6[1] = "0000000000001110";
		TEST_in7[1] = "0000000000001111";
		TEST_in8[1] = "0000000000010000";
		TEST_sel[1] = 7;
	}

};



int sc_main(int argc, char* argv[]) {

	TB test_mux("test_mux");
	cout << "Start test MUX" << endl;

	sc_start();

	cout << "End test MUX" << endl;  

	return 0;

}
