#include <systemc.h>
#include <string>
#include "not.hpp"

using namespace std;

SC_MODULE(TB){

   public:	

	sc_signal<sc_lv<16> >	in1_test;
	sc_signal<sc_lv<16> >	out1_test;

	NOT_MODULE	TB_NOT_MODULE;
	

	SC_CTOR(TB):TB_NOT_MODULE("TB_NOT_MODULE") {

		SC_THREAD(test_not);
		TB_NOT_MODULE.NOT_in1(this -> in1_test);
		TB_NOT_MODULE.NOT_out1(this -> out1_test);
		TEST_in_value();	
	}   

   private:

	static const int N_TEST = 3;
	sc_lv<16> TEST_in[N_TEST], TEST_out[N_TEST];


	void test_not(){

		for (unsigned j=0;j<N_TEST;j++) {

			in1_test.write(TEST_in[j]);
			cout << "NOT_MODULE with input: " << TEST_in[j] <<endl;

			wait(5,SC_NS);	

			TEST_out[j] = out1_test.read();

			cout << "return output NOT: "<<TEST_out[j] <<endl;
			cout << "   " <<endl;
		}	
	};

	void TEST_in_value() {

		TEST_in[0] = "1111111111111111";
		TEST_in[1] = "0000000000000000";
		TEST_in[2] = "1000000000000001";

	};

};



int sc_main(int argc, char* argv[]) {

	TB test_not("test_not");
	cout << "Start test NOT" << endl << endl;

	sc_start();

	cout << "End test NOT" << endl;  

	return 0;
}
