#include <systemc.h>
#include <string>
#include <math.h>
#include "and.hpp"

using namespace std;

SC_MODULE(TB){

   public:	

	sc_signal<sc_lv<16> >	in1_test;
	sc_signal<sc_lv<16> >	in2_test;
	sc_signal<sc_lv<16> >	out1_test;

	AND_MODULE	TB_AND_MODULE;
	

	SC_CTOR(TB):TB_AND_MODULE("TB_AND_MODULE"){

		SC_THREAD(test_and);
		TB_AND_MODULE.AND_in1(this -> in1_test);
		TB_AND_MODULE.AND_in2(this -> in2_test);
		TB_AND_MODULE.AND_out1(this -> out1_test);
		TEST_in_value();	
	}   

   private:

	static const int N_TEST = 2;
	sc_lv<16> TEST_in1[N_TEST], TEST_in2[N_TEST], TEST_out1[N_TEST];


	void test_and(){

		for (unsigned i=0;i<N_TEST;i++) {

			in1_test.write(TEST_in1[i]);
			cout << "AND_MODULE in1_test: " << TEST_in1[i] <<endl;
			in2_test.write(TEST_in2[i]);
			cout << "AND_MODULE in2_test: " << TEST_in2[i] <<endl;
						
			wait(5,SC_NS);	

			TEST_out1[i] = out1_test.read();

			cout << "return output AND:   "<<TEST_out1[i] <<endl;
			cout << "   " <<endl;
		}
	}	


	void TEST_in_value() {

		TEST_in1[0] = "1011111101110111";
		TEST_in2[0] = "1111111111111111";
		
		TEST_in1[1] = "1011111111110111";
		TEST_in2[1] = "0000111111111111";

	}

};



int sc_main(int argc, char* argv[]) {

	TB test_and("test_and");
	cout << "Start test AND" << endl;

	sc_start();

	cout << "End test AND" << endl;  

	return 0;

}
