#include <systemc.h>
#include <string>
#include <math.h>
#include "ALUv1_0.hpp"

using namespace std;

SC_MODULE(TB){

   public:	

	sc_signal<sc_lv<16> >	in1_test,in2_test;
	sc_signal<sc_lv<16> >	out1_test;
	
	sc_signal<sc_lv<3> >  sel_test;
	sc_signal<bool >  op_sel_test;

	ALU_MODULE	TB_ALU_MODULE;
	

	SC_CTOR(TB):TB_ALU_MODULE("TB_ALU_MODULE"){

		SC_THREAD(test_ALU);
		TB_ALU_MODULE.ALU_in1(this -> in1_test);
		TB_ALU_MODULE.ALU_in2(this -> in2_test);
		TB_ALU_MODULE.ALU_sel(this -> sel_test);
		TB_ALU_MODULE.ALU_op_sel(this -> op_sel_test);		
		TB_ALU_MODULE.ALU_out1(this -> out1_test);
		TEST_in_value();	
	}   

   private:

	static const int N_TEST = 3;
	sc_lv<16> TEST_in1[N_TEST], TEST_in2[N_TEST], TEST_out1[N_TEST];
	sc_lv<3>  TEST_sel[N_TEST];
	bool      TEST_op_sel[N_TEST];	


	void test_ALU(){

		for (unsigned i=0;i<N_TEST;i++) {

			sel_test.write(TEST_sel[i]);
			op_sel_test.write(TEST_op_sel[i]);
			cout << "op_sel_test and ALU sel_test: " << TEST_op_sel[i] << TEST_sel[i]<<endl;
			cout << " sel_test = 000 : AND "<<endl;
			cout << " sel_test = 001 : OR "<<endl;
			cout << " sel_test = 010 : NOT "<<endl;
			cout << " sel_test = 011 : XOR "<<endl;
			cout << " sel_test = 100 and op_sel_test = 0 : SHIFTER SX "<<endl;
			cout << " sel_test = 100 and op_sel_test = 1 : SHIFTER DX "<<endl;
			cout << " sel_test = 101 and op_sel_test = 0 : ADDER "<<endl;
			cout << " sel_test = 101 and op_sel_test = 1 : SUB "<<endl;
			cout << " sel_test = 110 : NAND "<<endl;
			cout << " sel_test = 111 : NOR "<<endl;
			in1_test.write(TEST_in1[i]);
			cout << "ALU in1_test: " << TEST_in1[i] <<endl;
			in2_test.write(TEST_in2[i]);
			cout << "ALU in2_test: " << TEST_in2[i] <<endl;
			
			wait(5,SC_NS);	

			TEST_out1[i] = out1_test.read();

			cout << "return output ALU: "<< TEST_out1[i] <<endl;
			cout << " " <<endl;
		}	
	}


	void TEST_in_value() {

		//Test 1
		TEST_in1[0] = 7;
		TEST_in2[0] = 73;

		TEST_sel[0] = "110";
		TEST_op_sel[0] = true;
		
		//Test 2		
		TEST_in1[1] = 7;
		TEST_in2[1] = 3;

		TEST_sel[1] = "101";
		TEST_op_sel[1] = false;

		//Test 3		
		TEST_in1[2] = 7;
		TEST_in2[2] = 3;

		TEST_sel[2] = "111";
		TEST_op_sel[2] = false;
	}

};



int sc_main(int argc, char* argv[]) {

	TB test_ALU("test_ALU");
	cout << "Start test ALU" << endl;

	sc_start();

	cout << "End test ALU" << endl;  

	return 0;

}
