#include <systemc.h>
#include <string>
#include <math.h>
#include "shifter.hpp"

using namespace std;

SC_MODULE(TB){

   public:	

	sc_signal<sc_lv<16> >	in1_test;
	sc_signal<sc_lv<16> >	out1_test;
	
	sc_signal<bool>  sel_test;

	SHIFTER_MODULE	TB_SHIFTER_MODULE;
	

	SC_CTOR(TB):TB_SHIFTER_MODULE("TB_SHIFTER_MODULE"){

		SC_THREAD(test_shifter);
		TB_SHIFTER_MODULE.SHIFTER_in1(this -> in1_test);
		TB_SHIFTER_MODULE.SHIFTER_sel(this -> sel_test);		
		TB_SHIFTER_MODULE.SHIFTER_out1(this -> out1_test);
		TEST_in_value();	}   

   private:

	static const int N_TEST = 2;
	sc_lv<16> TEST_in1[N_TEST], TEST_out1[N_TEST];
	bool      TEST_sel[N_TEST];


	void test_shifter(){

		for (unsigned i=0;i<N_TEST;i++) {

			in1_test.write(TEST_in1[i]);
			cout << "SHIFTER_MODULE in1_test: " << TEST_in1[i] <<endl;
			sel_test.write(TEST_sel[i]);
			cout << "SHIFTER_MODULE sel_test (0 left; 1 right): " << TEST_sel[i] <<endl;
			
			wait(5,SC_NS);	

			TEST_out1[i] = out1_test.read();

			cout << "return output SHIFTER: "<< TEST_out1[i] <<endl;
			cout << " " <<endl;	
		}	
	}


	void TEST_in_value() {

		//Test shift sx, sel = 0
		TEST_in1[0] = "1000000000000001";
		TEST_sel[0] = 0;
		
		//Test shift dx, sel = 1 		
		TEST_in1[1] = "1000000000000001";
		TEST_sel[1] = 1;
	}

};



int sc_main(int argc, char* argv[]) {

	TB test_shifter("test_shifter");
	cout << "Start test SHIFTER" << endl;

	sc_start();

	cout << "End test SHIFTER" << endl;  

	return 0;

}

