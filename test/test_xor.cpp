#include <systemc.h>
#include <string>
#include <math.h>
#include "xor.hpp"

using namespace std;

SC_MODULE(TB){

   public:	

	sc_signal<sc_lv<16> >	in1_test;
	sc_signal<sc_lv<16> >	in2_test;
	sc_signal<sc_lv<16> >	out1_test;

	XOR_MODULE	TB_XOR_MODULE;
	

	SC_CTOR(TB):TB_XOR_MODULE("TB_XOR_MODULE"){

		SC_THREAD(test_xor);
		TB_XOR_MODULE.XOR_in1(this -> in1_test);
		TB_XOR_MODULE.XOR_in2(this -> in2_test);
		TB_XOR_MODULE.XOR_out1(this -> out1_test);
		TEST_in_value();	}   

   private:

	static const int N_TEST = 2;
	sc_lv<16> TEST_in1[N_TEST], TEST_in2[N_TEST], TEST_out1[N_TEST];


	void test_xor(){

		for (unsigned i=0;i<N_TEST;i++) {

			in1_test.write(TEST_in1[i]);
			cout << "XOR_MODULE in1_test: " << TEST_in1[i] <<endl;
			in2_test.write(TEST_in2[i]);
			cout << "XOR_MODULE in2_test: " << TEST_in2[i] <<endl;
						
			wait(5,SC_NS);	

			TEST_out1[i] = out1_test.read();

			cout << "return output XOR:   "<<TEST_out1[i] <<endl;
			cout << "   " <<endl;
		}	
	}


	void TEST_in_value() {

		TEST_in1[0] = "1011111101110111";
		TEST_in2[0] = "1111111111111111";
		
		TEST_in1[1] = "1011111111110111";
		TEST_in2[1] = "0000111111111111";

	}

};



int sc_main(int argc, char* argv[]) {

	TB test_xor("test_xor");
	cout << "Start test XOR" << endl;

	sc_start();

	cout << "End test XOR" << endl;  

	return 0;

}
