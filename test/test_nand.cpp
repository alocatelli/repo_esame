#include <systemc.h>
#include <string>
#include <math.h>
#include "nand.hpp"

using namespace std;

SC_MODULE(TB){

   public:	

	sc_signal<sc_lv<16> >	in1_test,in2_test;
	sc_signal<sc_lv<16> >	out1_test;
	

	NAND_MODULE	TB_NAND_MODULE;
	

	SC_CTOR(TB):TB_NAND_MODULE("TB_NAND_MODULE"){

		SC_THREAD(test_NAND);
		TB_NAND_MODULE.NAND_in1(this -> in1_test);
		TB_NAND_MODULE.NAND_in2(this -> in2_test);
		TB_NAND_MODULE.NAND_out1(this -> out1_test);
		TEST_in_value();	
	}   

   private:

	static const int N_TEST = 2;
	sc_lv<16> TEST_in1[N_TEST], TEST_in2[N_TEST], TEST_out1[N_TEST];

	void test_NAND(){

		for (unsigned i=0;i<N_TEST;i++) {


			in1_test.write(TEST_in1[i]);
			cout << "NAND in1_test: " << TEST_in1[i] <<endl;
			in2_test.write(TEST_in2[i]);
			cout << "NAND in2_test: " << TEST_in2[i] <<endl;
			
			wait(5,SC_NS);	

			TEST_out1[i] = out1_test.read();

			cout << "return output NAND: "<< TEST_out1[i] <<endl;
			cout << " " <<endl;
		}	
	}


	void TEST_in_value() {

		//Test 1
		TEST_in1[0] = 7;
		TEST_in2[0] = 3;

		
		//Test 2		
		TEST_in1[1] = 5;
		TEST_in2[1] = 1;


	}

};



int sc_main(int argc, char* argv[]) {

	TB test_NAND("test_NAND");
	cout << "Start test NAND" << endl;

	sc_start();

	cout << "End test NAND" << endl;  

	return 0;

}
